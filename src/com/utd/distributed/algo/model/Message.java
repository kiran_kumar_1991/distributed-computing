package com.utd.distributed.algo.model;

public class Message {

	private int nodeId;
	private int hopCount;
	private MESSAGE_DIRECTION direction;
	private int currentRound;
	private int currentSourceNodeId;

	public Message(int nodeId, int hopCount, MESSAGE_DIRECTION direction, int currentRound) {
		super();
		this.nodeId = nodeId;
		this.hopCount = hopCount;
		this.direction = direction;
		this.currentRound = currentRound;
		this.currentSourceNodeId = nodeId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public int getHopCount() {
		return hopCount;
	}

	public void decrementHopCount() {
		hopCount--;
	}

	public MESSAGE_DIRECTION getDirection() {
		return direction;
	}

	public void incrementRound() {
		currentRound++;
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public int getCurrentSourceNodeId() {
		return currentSourceNodeId;
	}

	public void setCurrentSourceNodeId(int currentSourceNodeId) {
		this.currentSourceNodeId = currentSourceNodeId;
	}

	public void reverseMsgDirection() {
		direction = MESSAGE_DIRECTION.IN;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[NodeId:").append(nodeId).append(" HopCount:").append(hopCount).append(" Direction:")
				.append(direction).append(" Round:").append(currentRound).append(" currentSource:")
				.append(currentSourceNodeId).append("]");
		return builder.toString();
	}

}
