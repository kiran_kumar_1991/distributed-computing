package com.utd.distributed.algo.model;

/*
 * Specifies whether the message is an incoming or outgoing message with respect to a particular node. 
 */
public enum MESSAGE_DIRECTION {

	IN, OUT

}
