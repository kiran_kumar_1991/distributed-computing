package com.utd.distributed.algo.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Node implements Runnable {

	private int number;
	private int id;
	private Channel leftChannel;
	private Channel rightChannel;
	private int phase = 0;
	private boolean isLeader = false;
	private int currentRound;
	private int noOfMsgsGotBack = 2;
	private Logger logger = LogManager.getLogger(Node.class);
	private boolean isRoundComplete = false;

	/*
	 * number - Node number id - Node ID, it should be unique left - Left
	 * Channel right - Right Channel
	 */
	public Node(int number, int id, Channel left, Channel right) {
		super();
		this.number = number;
		this.id = id;
		this.leftChannel = left;
		this.rightChannel = right;
		this.currentRound = 0;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (!isRoundComplete()) {
					executeRound();
					setRoundComplete(true);
				} else {
					Thread.sleep(300);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/*
	 * A round involves reading all the incoming messages, process it to
	 * generate new states and place the generated messages in the appropriate
	 * channel
	 */
	private void executeRound() {
		processIncomingMessages();
		generateMessages();
	}

	/*
	 * Generate new messages, only if the previously sent messages in both the
	 * directions are received back. Initially, all the nodes send out messages
	 * in both the directions
	 */
	private void generateMessages() {
		if (noOfMsgsGotBack == 2) {
			incrementPhase();
			sendMessages();
			noOfMsgsGotBack = 0;
		} else {
			logger.debug("Node number:{} with node id:{} has no message to send", this.number, this.id);
		}
	}

	/*
	 * Create messages and place them in the left and right channels
	 */
	private void sendMessages() {
		Message leftChannelMsg = new Message(this.getId(), this.computeHopCount(), MESSAGE_DIRECTION.OUT,
				this.getCurrentRound() + 1);
		leftChannel.putMessage(leftChannelMsg);

		Message rightChannelMsg = new Message(this.getId(), this.computeHopCount(), MESSAGE_DIRECTION.OUT,
				this.getCurrentRound() + 1);
		rightChannel.putMessage(rightChannelMsg);
	}

	/*
	 * Returns an integer. Hop count = 2 ^ (Phase)
	 */
	private int computeHopCount() {
		return (int) Math.pow(2, phase);
	}

	/*
	 * Read the messages from the left channel and right channel. Process the
	 * messages, if there is any.
	 */
	private void processIncomingMessages() {
		Message leftChannelMsg = leftChannel.getMessage(this.getId(), this.getCurrentRound());
		if (leftChannelMsg != null)
			processIncomingMessage(leftChannelMsg, CHANNEL_DIRECTION.LEFT);

		Message rightChannelMsg = rightChannel.getMessage(this.getId(), this.getCurrentRound());
		if (rightChannelMsg != null)
			processIncomingMessage(rightChannelMsg, CHANNEL_DIRECTION.RIGHT);
	}

	private void processIncomingMessage(Message msg, CHANNEL_DIRECTION channelDirection) {
		if (msg.getDirection() == MESSAGE_DIRECTION.OUT) {
			// Select the node as leader, if it receives it's own message with
			// direction as OUT.
			if (msg.getNodeId() == this.id) {
				this.isLeader = true;
				logger.debug("Received my own msg. Electing node id:{} as leader", this.getId());
				return;
			} else if (msg.getNodeId() < id) {
				// Drop the message, if the UID is lesser than the current
				// node's UID.
				logger.debug("Dropping message as id:{} is lesser than nodeId:{}", msg.getNodeId(), id);
			} else if (msg.getNodeId() > id) {
				if (msg.getHopCount() == 1) {
					// Reverse the direction, if the hop count equals 1.
					logger.debug("Reversing the direction of msg: {}", msg);
					returnTheMsg(msg, channelDirection);
				} else {
					// Keep forwarding the message.
					forwardTheMsg(msg, channelDirection, true);
				}
			}
		} else if (msg.getDirection() == MESSAGE_DIRECTION.IN) {
			// If the message, was sent by the same node then increment the
			// message got back.
			if (msg.getNodeId() == this.getId()) {
				noOfMsgsGotBack++;
			} else {
				// Keep forwarding the message until it reaches the owner node.
				forwardTheMsg(msg, channelDirection, false);
			}
		} else {
			logger.error("Unhandled message direction: {}", msg.getDirection());
		}
	}

	/*
	 * Forwards the message to the next node in the same direction specified.
	 * According to the HS Algorithm, the hop count should not be decremented
	 * for "incoming" messages. So, the method takes a flag to determine whether
	 * the hop count should be decremented or not. The argument passed should be
	 * "false" for incoming messages
	 */
	private void forwardTheMsg(Message msg, CHANNEL_DIRECTION channelDirection, boolean shouldDecrementHopCount) {
		msg.incrementRound();
		msg.setCurrentSourceNodeId(this.getId());
		if (shouldDecrementHopCount)
			msg.decrementHopCount();
		if (channelDirection == CHANNEL_DIRECTION.LEFT) {
			rightChannel.putMessage(msg);
		} else if (channelDirection == CHANNEL_DIRECTION.RIGHT) {
			leftChannel.putMessage(msg);
		} else {
			logger.error("Unhandled channel direction:{}", channelDirection);
		}

	}

	/*
	 * Returns the message in the direction opposite to that of the channel
	 * direction specified in the argument.
	 */
	private void returnTheMsg(Message msg, CHANNEL_DIRECTION channelDirection) {
		msg.reverseMsgDirection();
		msg.setCurrentSourceNodeId(this.getId());
		msg.incrementRound();
		if (channelDirection == CHANNEL_DIRECTION.LEFT) {
			leftChannel.putMessage(msg);
		} else if (channelDirection == CHANNEL_DIRECTION.RIGHT) {
			rightChannel.putMessage(msg);
		} else {
			logger.error("Unhandled channel direction:{}", channelDirection);
		}
	}

	public int getNumber() {
		return number;
	}

	public int getId() {
		return id;
	}

	public Channel getLeftChannel() {
		return leftChannel;
	}

	public Channel getRightChannel() {
		return rightChannel;
	}

	public Channel getLeft() {
		return leftChannel;
	}

	public Channel getRight() {
		return rightChannel;
	}

	public int getPhase() {
		return phase;
	}

	public boolean isLeader() {
		return isLeader;
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public void incrementCurrentRound() {
		this.currentRound++;
	}

	public void incrementPhase() {
		this.phase++;
	}

	public synchronized boolean isRoundComplete() {
		return isRoundComplete;
	}

	public synchronized void setRoundComplete(boolean isRoundComplete) {
		this.isRoundComplete = isRoundComplete;
	}

}
