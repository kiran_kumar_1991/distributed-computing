package com.utd.distributed.algo.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Channel {

	private List<Message> messages = new LinkedList<Message>();

	private Logger logger = LogManager.getLogger(Channel.class);

	/*
	 * Places the message in the channel
	 */
	public synchronized boolean putMessage(Message message) {
		messages.add(message);
		return true;
	}

	/*
	 * This method takes nodeId and the currentRound to fetch the message for a
	 * the specified node in the specified round from the list of messages
	 * present in the channel. Returns the message, if there's one. Else, it
	 * returns null
	 */
	public synchronized Message getMessage(int nodeId, int currentRound) {
		Iterator<Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message msg = iterator.next();
			if (msg.getCurrentSourceNodeId() != nodeId && msg.getCurrentRound() == currentRound) {
				iterator.remove();
				return msg;
			}
		}
		logger.debug("No message in the channel for nodeNumber:{} for round:{}", nodeId, currentRound);
		return null;
	}

}
