package com.utd.distributed.algo;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.utd.distributed.algo.model.Node;
import com.utd.distributed.algo.util.InputParser;
import com.utd.distributed.algo.util.StringUtil;

public class Controller {

	private static final String INPUT_FILE_KEY = "input.dat";

	private static Logger logger = LogManager.getLogger(Controller.class);
	private static List<Thread> threads = new LinkedList<Thread>();
	private static List<Node> nodes;

	public static void main(String[] args) {
		long start = System.nanoTime();
		String inputFile = System.getProperty(INPUT_FILE_KEY);

		if (StringUtil.isNull(inputFile)) {
			logger.warn("Using default input file, since the property input.dat is not specified as VM argument");
			inputFile = "conf/input.dat";
		}

		InputParser parser = new InputParser(inputFile);
		parser.readInput();
		nodes = parser.getNodes();
		createThreads();
		Controller controller = new Controller();
		controller.startHSAlgorithm(nodes);
		long duration = System.nanoTime() - start;
		logger.debug("Total time taken: {}", TimeUnit.MILLISECONDS.convert(duration, TimeUnit.NANOSECONDS));
	}

	/*
	 * Create a separate thread for each node
	 */
	private static void createThreads() {
		for (Node node : nodes) {
			Thread thread = new Thread(node, "Node:" + node.getId());
			logger.debug("Created thread for {}", thread.getName());
			threads.add(thread);
		}
	}

	/*
	 * Runs HS Algorithm to elect leader in a ring of nodes. This algorithm has
	 * a time complexity of O(N) and message complexity of O(NlogN)
	 */
	private void startHSAlgorithm(List<Node> nodes) {
		int roundNumber = 0;
		try {
			startThreads();
			while (true) {
				waitForThreadsToComplete();
				if (checkIfLeaderIsElected(nodes)) {
					terminateThreads();
					break;
				}
				roundNumber++;
				setRoundNumberForNodes(roundNumber);
			}
		} catch (Exception e) {
			logger.error(e);
		}

	}

	private void terminateThreads() {
		for (Thread thread : threads) {
			try {
				thread.interrupt();
			} catch (Exception e) {
				logger.error(e);
			}
		}

	}

	/*
	 * Increment round number for all the nodes
	 */
	private void setRoundNumberForNodes(int roundNumber) {
		for (Node node : nodes) {
			synchronized (node) {
				node.setRoundComplete(false);
				node.incrementCurrentRound();
			}
		}

	}

	/*
	 * Wait until all the threads complete their execution
	 */

	private void waitForThreadsToComplete() throws InterruptedException {
		for (Node node : nodes) {
			logger.debug("Waiting for node:{} to complete round", node.getId());
			while (true) {
				synchronized (node) {
					if (node.isRoundComplete() == true)
						break;
				}
				Thread.sleep(150);
			}
			logger.debug("Node:{} completed the round", node.getId());
		}
	}

	/*
	 * Start all the threads
	 */
	private void startThreads() {
		for (Thread thread : threads) {
			thread.start();
		}
	}

	/*
	 * Returns true when it sees the first node claiming itself to be the
	 * leader. Returns false, if none of the node claims to be the leader
	 */
	private boolean checkIfLeaderIsElected(List<Node> nodes) {
		for (Node node : nodes) {
			if (node.isLeader() == true) {
				logger.info("Node:{} with UID:{} is the leader", node.getNumber(), node.getId());
				return true;
			}
		}
		return false;
	}

}
