package com.utd.distributed.algo.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.utd.distributed.algo.model.Channel;
import com.utd.distributed.algo.model.Node;

public class InputParser {

	private String fileLocation;
	private List<Node> nodes = new LinkedList<Node>();

	private static Logger logger = LogManager.getLogger(InputParser.class);

	public InputParser(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	// Read the configuration from the file and configure the nodes
	public void readInput() {
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;

		try {
			logger.info("Using the conf file located at: {}", fileLocation);
			fileReader = new FileReader(fileLocation);
			bufferedReader = new BufferedReader(fileReader);
			String line = bufferedReader.readLine();
			int noOfNodes = Integer.parseInt(line);
			for (int i = 0; i < noOfNodes; i++) {
				line = bufferedReader.readLine();
				validateNodeId(line);
				int nodeId = Integer.parseInt(line);

				// Last node
				if (i == noOfNodes - 1)
					createNode(i, nodeId, true);
				else
					createNode(i, nodeId, false);
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			try {
				bufferedReader.close();
				fileReader.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	private void validateNodeId(String line) throws Exception {
		if (StringUtil.isNull(line))
			throw new Exception("ProcessId is null/empty");
	}

	private void createNode(int i, int nodeId, boolean isLastNode) {
		Channel left = null, right = null;

		if (nodes.size() == 0) {
			// This is the first node. So, left channel will not exist.
			left = new Channel();
		} else {
			// The previous node's right channel will be the new node's left
			// channel
			left = nodes.get(nodes.size() - 1).getRightChannel();
		}

		if (isLastNode) {
			// If this is the last node, then the node's right channel will be
			// the first node's left channel
			right = nodes.get(0).getLeftChannel();
		} else {
			// Create a new channel for the node.
			right = new Channel();
		}
		Node node = new Node(i, nodeId, left, right);
		nodes.add(node);
	}

	public List<Node> getNodes() {
		if (nodes.size() == 0) {
			logger.error("Either the input file has 0 nodes or the API was called before readInput() API is called");
			return nodes;
		}
		return nodes;
	}
}
